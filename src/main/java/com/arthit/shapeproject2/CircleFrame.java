/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shapeproject2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Arthit
 */
public class CircleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Circle");
        frame.setSize(310, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lbRadius = new JLabel("radius", JLabel.CENTER);
        lbRadius.setSize(50, 20);
        lbRadius.setLocation(5, 5);
        lbRadius.setBackground(Color.yellow);
        lbRadius.setOpaque(true);
        frame.add(lbRadius);

        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        frame.add(txtRadius);

        JButton btCal = new JButton("Calculate");
        btCal.setSize(100, 20);
        btCal.setLocation(120, 5);
        frame.add(btCal);

        final JLabel lbResult = new JLabel("Circle radius = ??? area =??? Perimeter = ???");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(300, 50);
        lbResult.setLocation(0, 50);
        lbResult.setBackground(Color.orange);
        lbResult.setOpaque(true);
        frame.add(lbResult);

        btCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strValue = txtRadius.getText();
                    double value = Double.parseDouble(strValue);
                    if (value == 0) {
                        JOptionPane.showMessageDialog(frame, "Please input radius more than 0 !!", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    Circle circle = new Circle(value);
                    lbResult.setText("Circle redius: " + String.format("%.2f", circle.getRadius()) + "  Area: " + String.format("%.2f", circle.calArea()) + "  Perimeter: " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Please input number.", "Error", JOptionPane.ERROR_MESSAGE);

                }
            }

        });

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
