/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shapeproject2;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Arthit
 */
public class TriangleFrame extends JFrame {

    JLabel lbTitle;
    JButton bt;
    final JTextField field;
    JLabel result;
    JLabel txtTri;
    JButton btDelete;
    int condition = -1;
    double baseTri;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(500, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setLayout(null);

        lbTitle = new JLabel("Triangle", JLabel.CENTER);
        lbTitle.setSize(100, 50);
        lbTitle.setFont(new Font("Verdana", Font.BOLD, 20));
        lbTitle.setLocation(188, 2);
//      lbTitle.setBackground(Color.red);
//      lbTitle.setOpaque(true);
        this.add(lbTitle);

        txtTri = new JLabel("Base:", JLabel.CENTER);
        txtTri.setSize(100, 50);
        txtTri.setFont(new Font("Verdana", Font.BOLD, 20));
        txtTri.setLocation(2, 52);
        this.add(txtTri);

        field = new JTextField();
        field.setSize(300, 30);
        field.setLocation(110, 65);
        this.add(field);

        bt = new JButton("Next");
        bt.setSize(160, 30);
        bt.setLocation(160, 115);
        this.add(bt);

        result = new JLabel("Ready.", JLabel.CENTER);
        result.setSize(400, 30);
        result.setFont(new Font("Verdana", Font.BOLD, 14));
        result.setLocation(41, 167);
//      result.setBackground(Color.red);
//      result.setOpaque(true);
        this.add(result);

        btDelete = new JButton("Clr");
        btDelete.setFont(new Font("Verdana", Font.BOLD, 15));
        btDelete.setSize(60, 25);
        btDelete.setLocation(415, 67);
        this.add(btDelete);

        btDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                condition = -1;
                txtTri.setText("Base:");
                bt.setText("Next");
                field.setText(null);
                result.setText("Ready.");

            }

        }
        );

        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (condition == -1) {
                        baseTri = Double.parseDouble(field.getText());
                        if (baseTri == 0) {
                            JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number more than 0. !!!", "Error", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        txtTri.setText("High:");
                        result.setText("Input high of triangle.");
                        bt.setText("Calculate");
                        condition = -2;
                        field.setText(null);
                    } else if (condition == -2) {
                        double highTri = Double.parseDouble(field.getText());
                        if (highTri == 0) {
                            JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number more than 0. !!!", "Error", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        Triangle triangle = new Triangle(baseTri, highTri);
                        result.setText("Triangle Base: " + String.format("%.2f", baseTri) + "  High: " + String.format("%.2f", highTri) + "  Area: " + String.format("%.2f", triangle.calArea()));
                        bt.setText("Next");
                        txtTri.setText("Base:");
                        condition = -1;
                        field.setText(null);
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number. !!!", "Error", JOptionPane.ERROR_MESSAGE);
                }

            }

        }
        );

        this.setVisible(true);
    }

    public static void main(String[] args) {
        TriangleFrame triangle = new TriangleFrame();
    }
}
