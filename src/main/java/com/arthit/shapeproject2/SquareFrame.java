/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shapeproject2;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Arthit
 */
public class SquareFrame extends JFrame {

    JLabel lbTitle;
    final JTextField field;
    JLabel lbDimention;
    JButton bt;
    JLabel result;

    public SquareFrame() {
        super("Square");
        this.setSize(500, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setLayout(null);

        lbTitle = new JLabel("Square");
        lbTitle.setFont(new Font("Verdana", Font.BOLD, 20));
        lbTitle.setSize(300, 80);
//      lbtitle.setBackground(Color.red);
//      lbtitle.setOpaque(true);
        lbTitle.setLocation(200, 0);
        this.add(lbTitle);

        lbDimention = new JLabel("Side:");
        lbDimention.setSize(300, 80);
        lbDimention.setLocation(80, 80);
        lbDimention.setFont(new Font("Verdana", Font.BOLD, 20));
        this.add(lbDimention);

        field = new JTextField();
        field.setSize(200, 30);
        field.setLocation(150, 108);
        this.add(field);

        bt = new JButton("Calculate");
        bt.setSize(150, 30);
        bt.setLocation(170, 160);
        this.add(bt);

        result = new JLabel("Result", JLabel.CENTER);
        result.setSize(500, 100);
//      result.setBackground(Color.red);
//      result.setOpaque(true);
        result.setLocation(-5, 190);
        result.setFont(new Font("Verdana", Font.BOLD, 14));
        this.add(result);

        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double side = Double.parseDouble(field.getText());
                    if (side == 0) {
                        JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number more than 0. !!!", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    Square square = new Square(side);
                    result.setText("Square side: " + String.format("%.2f", square.getSide()) + "  Area: " + String.format("%.2f", square.calArea()) + "  Perimeter: " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error Please input number. !!!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

        }
        );

        this.setVisible(true);

    }

    public static void main(String[] args) {
        SquareFrame square = new SquareFrame();

    }
}
