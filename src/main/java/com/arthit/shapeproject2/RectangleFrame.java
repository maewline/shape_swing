/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shapeproject2;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Modifier;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Arthit
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 500);
        frame.setLayout(null);
        frame.setBackground(Color.yellow);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JLabel labelname = new JLabel("Rectangle", JLabel.CENTER);
        frame.add(labelname);
        labelname.setSize(200, 50);
        labelname.setLocation(145, 5);
        //labelname.setBackground(Color.red);
        labelname.setOpaque(true);
        labelname.setFont(new Font("Verdana", Font.BOLD, 18));

        JTextField field = new JTextField();
        field.setSize(200, 30);
        field.setLocation(145, 60);
        frame.add(field);

        JLabel labelname2 = new JLabel("Wide", JLabel.CENTER);
        frame.add(labelname2);
        labelname2.setSize(200, 50);
        labelname2.setLocation(5, 45);
        //labelname.setBackground(Color.red);
        labelname2.setOpaque(true);
        labelname2.setFont(new Font("Verdana", Font.BOLD, 18));

        JTextField field2 = new JTextField();
        field2.setSize(200, 30);
        field2.setLocation(145, 95);
        frame.add(field2);

        JLabel labelname3 = new JLabel("Hight", JLabel.CENTER);
        frame.add(labelname3);
        labelname3.setSize(200, 50);
        labelname3.setLocation(5, 80);
        //labelname.setBackground(Color.red);
        labelname3.setOpaque(true);
        labelname3.setFont(new Font("Verdana", Font.BOLD, 18));

        JButton bt = new JButton("Calculate");
        bt.setSize(100, 30);
        bt.setLocation(190, 140);
        frame.add(bt);

        final JLabel result = new JLabel("Result ", JLabel.CENTER);
        result.setSize(500, 30);
        result.setLocation(0, 190);
//        result.setBackground(Color.red);
//        result.setOpaque(true);
        result.setFont(new Font("Verdana", Font.BOLD, 18));
        frame.add(result);

        final JLabel result2 = new JLabel("is ", JLabel.CENTER);
        result2.setSize(500, 30);
        result2.setLocation(0, 220);
//        result.setBackground(Color.red);
//        result.setOpaque(true);
        result2.setFont(new Font("Verdana", Font.BOLD, 18));
        frame.add(result2);

        final JLabel result3 = new JLabel("... ", JLabel.CENTER);
        result3.setSize(500, 30);
        result3.setLocation(0, 250);
//        result.setBackground(Color.red);
//        result.setOpaque(true);
        result3.setFont(new Font("Verdana", Font.BOLD, 18));
        frame.add(result3);

        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String w = field.getText();
                    String h = field2.getText();
                    double wide = Double.parseDouble(w);
                    double high = Double.parseDouble(h);
                    if (wide == 0 || high == 0) {
                        JOptionPane.showMessageDialog(frame, "Error: Please input number more than 0.!!!", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    Rectangle rectangle = new Rectangle(wide, high);
                    result.setText("Rectangle Wide: " + wide + "  Rectangle High: " + high);
                    result2.setText("Area: " + String.format("%.2f", rectangle.calArea()));
                    result3.setText("Perimeter: " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Please Input Number.", "Error Krab Khon Pee.!!!", JOptionPane.ERROR_MESSAGE);
                }
            }

        }
        );
        frame.setVisible(true);
    }
}
